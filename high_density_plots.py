import numpy as np
from influxdb import InfluxDBClient
import mysql.connector

def high_density_plots(env,app,time):
    client = InfluxDBClient(host='192.168.12.103', port=8086, username='admin', password='pandasht')
    client.switch_database('ifEvents')

    #pegando todos dados do influx com os parametros env,app e time
    env_data = []
    result = client.query(f"SELECT measure FROM {env} WHERE application='{app}' AND time > now() - {time} GROUP BY application,node_name, io ")
    if('series' in result.raw):
        for i in result.raw['series']:
            env_data.append(i)

    #z score
    for data in env_data:
        measures = []
        for measure in data['values']:
            measures.append(measure[1])
        mean = np.mean(measures)
        std = np.std(measures)
        score = [((i-mean)/std)+5 for i in measures]
        for i,measure in enumerate(data['values']):
            if(not np.isnan(score[i])):
                measure[1] = score[i]
            else:
                measure[1] = 0.0
    
    #formatando os dados para escrita
    json = []
    count = 0
    for i in env_data:
        values = i.pop('values')
        i.pop('columns')
        for value in values:   
            aux = i.copy()
            aux['fields'] = {'value':(value[1]+5*count)}
            aux['measurement'] = aux.pop('name')
            aux['time'] = value[0]
            json.append(aux)   
        count = count+1

    #escrevendo os dados no influx  
    client.switch_database('high_density_plots') 
    print(client.write_points(json))


def forecast():
    con = mysql.connector.connect(
        host='localhost',
        user='applications',
        password='ht102030',
        database='Forecasts'
    )
    cursor = con.cursor()

    #pegando todos nodes de dew point do influx
    client = InfluxDBClient(host='192.168.12.103', port=8086, username='admin', password='pandasht')
    client.switch_database('ifForecasts')
    nodes = client.query('select node_name from (select * from  /./ where info={thr})'.format(thr="'thr'"))
    nodes = [i[1] for i in nodes.raw['series'][0]['values']]


    #pegando todos dados e agrupando por node  
    forecasts,error = {},{}
    for node in nodes:
        cursor.execute(
            f"SELECT value,timestamp FROM forecast JOIN sensors ON forecast.sensor_id = sensors.id WHERE sensors.node = '{node}'")
        forecasts[f'{node}'] = cursor.fetchall()
        cursor.execute(
             f"SELECT value,timestamp FROM error JOIN sensors ON error.sensor_id = sensors.id WHERE sensors.node = '{node}'")
        error[f'{node}']= cursor.fetchall()       
    cursor.close()

    #fazendo a normalizacao dos dados de erro futuro e formatando para enviar ao influx
    json = []
    node_count = 0
    for node in forecasts:
        count = 0
        measures = []
        for data in forecasts[node]:
            measures.append(data[0])

        mean = np.mean(measures)
        std = np.std(measures)
        score = [((i-mean)/std)+5 for i in measures]
        for i,measure in enumerate(score):
            if(np.isnan(measure)):
                score[i] = 0.0  

        for data in forecasts[node]:
            aux = {'measurement':'DEW_POINT'}
            aux['fields']= {'forecast_error':(score[count]+node_count*5)}
            aux['tags'] = {'node_name':node}
            aux['time'] = data[1].strftime('%Y-%m-%dT%H:%M:%S')     
            json.append(aux)
            count = count +1
        node_count = node_count+1

    #fazendo a normalizacao dos dados de erro e formatando para enviar ao influx
    node_count = 0
    for node in error:
        count = 0
        measures = []
        for data in error[node]:
            measures.append(data[0])

        mean = np.mean(measures)
        std = np.std(measures)
        score = [((i-mean)/std)+5 for i in measures]
        for i,measure in enumerate(score):
            if(np.isnan(measure)):
                score[i] = 0.0  

        for data in error[node]:
            aux = {'measurement':'DEW_POINT'}
            aux['fields']= {'error':(score[count]+node_count*5)}
            aux['tags'] = {'node_name':node}
            aux['time'] = data[1].strftime('%Y-%m-%dT%H:%M:%S')     
            json.append(aux)
            count = count +1
        node_count = node_count+1

    client.switch_database('high_density_plots') 
    print(client.write_points(json))
   