from influxdb import InfluxDBClient
from pprint import pprint
import data_quality_analysis



def create_table(time):
    client = InfluxDBClient(host='192.168.12.103', port=8086, username='admin', password='pandasht')
    client.switch_database('ifEvents')

    #pegando do banco de dados todos environments
    result = client.query("show measurements")
    env_aux = [i[0] for i in result.raw['series'][0]['values']]
    env = {}

    #pegando do banco de dados todos apps de cada environment
    client.switch_database('ifConfigs') 
    for i in env_aux:
        result = client.query(f"SELECT application,measure FROM /./ WHERE app_env='{i}' GROUP BY application")
        app = set([i['values'][0][1] for i in result.raw['series']])
        env[i] = app

    for i in env:
        for j in env[i]:
            data_quality_analysis.write_data(i,j,time)

