from flask import Flask, request,redirect
import high_density_plots
import data_quality_analysis
import data_quality_analysis_table
import parser
app = Flask(__name__)

@app.route("/high_density_plots",methods=['GET'])
def high_density():  
    env = request.args.get('var-app_env')
    app = request.args.get('var-application')
    io = request.args.get('var-io')
    time = request.args.get('from')
    time2 = time.replace('now-','')
    high_density_plots.high_density_plots(env,app,time2)
    return redirect(f'grafana.if.htmicron.com.br:3000/d/GMLECHvnk/graficos-de-alta-densidade?orgId=1&var-app_env={env}&var-application={app}&var-io={io}&var-node=All&from={time}&to=now')
    

@app.route("/data_quality_analysis",methods=['GET'])
def data_quality():
    env = request.args.get('var-app_env')
    app = request.args.get('var-application')
    io = request.args.get('var-io')
    time = request.args.get('from')
    time2 = time.replace('now-','')
    data_quality_analysis.write_data(env,app,time2)
    return redirect(f'grafana.if.htmicron.com.br:3000/d/J4VP5RO7z/qualidade-dos-sinais?orgId=1&var-app_env={env}&var-application={app}&var-io={io}&var-node=All&from={time}&to=now')
    

@app.route("/data_quality_analysis_table",methods=['GET'])
def data_quality_table():
    time = request.args.get('from')
    time2 = time.replace('now-','')
    data_quality_analysis_table.create_table(time2)
    return redirect(f'grafana.if.htmicron.com.br:3000/d/saAhughnz/tabela-qualidade-dos-sinais?orgId=1')
   

@app.route("/error_high_density_plot",methods=['GET'])
def error_high_density():
    high_density_plots.forecast()
    return redirect('grafana.if.htmicron.com.br:3000/d/_XGVLBQWk/dew-point-forecast?orgId=1&refresh=10s')

@app.route("/spc",methods=['GET'])
def spc():
    env = request.args.get('var-app_env')
    app = request.args.get('var-application')
    node = request.args.get('var-node')
    if(node == 'All'):
        return redirect(f'grafana.if.htmicron.com.br:3000/d/O58URS1Gk/statistical-process-control?orgId=1&refresh=5s&var-Node={node}')
    channel = parser.get_channel(env,app,node)
    return redirect(f'grafana.if.htmicron.com.br:3000/d/O58URS1Gk/statistical-process-control?orgId=1&refresh=5s&var-Node={node}&var-Channel={channel}')

@app.route("/software_sensors",methods=['GET'])
def software_sensors():
    env = request.args.get('var-app_env')
    app = request.args.get('var-application')
    node = request.args.get('var-node')
    if(node == 'All'):
        return redirect(f'grafana.if.htmicron.com.br:3000/d/MOoapZoGk/software-sensors?orgId=1&refresh=5s&var-Node={node}')
    channel = parser.get_channel(env,app,node)
    return redirect(f'grafana.if.htmicron.com.br:3000/d/MOoapZoGk/software-sensors?orgId=1&refresh=5s&var-Node={node}&var-Channel={channel}')


app.run(debug=True,port=1722)
