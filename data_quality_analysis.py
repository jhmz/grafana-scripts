import numpy as np
from datetime import datetime, timedelta
from influxdb import InfluxDBClient


#write_data executa todas funcoes desse arquivo e escreve na tabela data_quality_analysis 
def write_data(env,app,time):
    #conectando ao influx
    client = InfluxDBClient(host='192.168.12.103', port=8086, username='admin', password='pandasht')
    client.switch_database('ifEvents')

    #convertendo a string de tempo para int em minutos
    time_int = 0        
    if(time[-1] == 'h'):                    #horas
        time_int = int(time[:-1])*60
    elif(time[-1] == 'd'):                  #dias
        time_int = int(time[:-1])*60*24
    elif(time[-1] == 'm'):                  #minutos
        time_int = int(time[:-1])

    #pegando todos os dados de acordo com o environment,aplicacao e tempo selecionado no grafana
    client.switch_database('ifEvents')
    env_data, json = [],[]
    result = client.query(f"SELECT measure FROM {env} WHERE application='{app}' AND time > now() - {time} GROUP BY application,node_name, io ")
    if('series' in result.raw):
        for i in result.raw['series']:
            env_data.append(i)    
    
    #aplicando todas funcoes e agrupando todos dados em um unico json para escrever no banco
    missing = data_missing(env_data,time_int)
    for i in missing:
        json.append(i)
    time_values = time_between_values(env_data)
    for i in time_values:
        json.append(i)
    out = outliers(env_data,time_int)
    for i in out:
        json.append(i)
    qf_cf = quantization_compression_factor(env_data,time_int)
    for i in qf_cf:
        json.append(i)
    window_missing(env_data,time_int)
    #escrevendo os dados no banco
    client.switch_database('data_quality_analysis') 
    print(client.write_points(json))
 

#checando os valores que faltam em relacao aotempode envio e formatando para enviar ao banco
def data_missing(env_data,time):
    json = []
    for data in env_data:
        len_data = len(data['values'])
        missing = (1 - len_data/time) * 100
        if(missing < 0):
            missing = 0.0
        data['fields'] = {'missing':missing}
        json.append(data)
    return json


#calculando o tempo de envio entre cada valor enviado ao banco de dados
def time_between_values(env_data):
    json= []
    for data in env_data:
        thirty = -1              #<=30sec
        more_than_thirty= 0     #>30sec and <60sec
        sixty = 0               #60sec
        more_than_sixty= 0      #>60sec and <=300sec
        three_hundred = 0       #>300

        #verifica o tempo de envio entre dados
        previous = datetime.strptime(data['values'][0][0],'%Y-%m-%dT%H:%M:%S.%fZ')
        for value in data['values']:
            current = datetime.strptime(value[0],'%Y-%m-%dT%H:%M:%S.%fZ')
            aux = current - previous
            aux = int(aux.total_seconds())
            if(aux <= 30):
                thirty = thirty+1
            elif(aux < 60):
                more_than_thirty = more_than_thirty +1
            elif(aux == 60):
                sixty = sixty +1
            elif(aux <300):
                more_than_sixty = more_than_sixty +1
            else:
                three_hundred = three_hundred +1
            previous = current

        data['fields']['thirty'] = thirty
        data['fields']['more_than_thirty']= more_than_thirty
        data['fields']['sixty'] = sixty
        data['fields']['more_than_sixty'] = more_than_sixty
        data['fields']['three_hundred'] = three_hundred
        json.append(data)
    return json

#detecta outliers utilizando um z-score
def outliers(env_data,time):
    json = []
    for data in env_data:
        outliers = 0
        values = []
        for value in data['values']:
            values.append(value[1])
        mean = np.mean(values)
        std = np.std(values)
        for value in values:
            z_score = (value - mean)/std
            if(np.abs(z_score) > 3):
                outliers = outliers +1
        len_data = len(data['values'])
        data['fields']['outliers'] = outliers/time * 100
        json.append(data)

    return json


#faz o calculodo fatorde compressao equantizacao
def quantization_compression_factor(env_data,time):
    json = []
    R = 15  
    for data in env_data:
        N = len(data['values'])
        x,y,z = [],[],[]
        yhatdot=np.ones(N-1)
        yhatddot=np.ones(N-1)
        for value in data['values']:
            x.append(value[1])

        P = np.ceil(np.log10(np.abs(x))) 
        for i,value in enumerate(x):
            y.append(value/10**P[i])
        for i,value in enumerate(x):
            z.append(y[i]*10**R)
        yhat= z.copy()

        #calculando primeira e segunda derivada
        for i in range(2,N-1):
            yhatdot[i]=(x[i] - x[i-1])/60
            yhatddot[i]=(yhat[i+1]-2*yhat[i]+yhat[i-1])/(time**2)

        #checando todos zeros no sinal
        n = len(yhatddot) - np.count_nonzero(yhatddot)
        m = N-n
        cf = 0
        #calculando o fator de compressao
        if(n==0):           #sem compressao
            cf = N/m
        else:
            if(m%2 == 0):   #par 
                cf =N/(m/2)
            else:           #impar
                cf=N/m

        #calculando o fator de quantizacao
        qf = 1
        Qlevel = np.min(np.abs(yhatdot))
        qf = Qlevel/np.std(x)
        if(np.isnan(qf)):
            qf = 0.0

        #formatando os dados para enviar ao banco
        data['measurement'] =data.pop('name')
        data['fields']['cf'] = cf
        data['fields']['qf'] = qf
        data['time'] = data['values'][-1][0]
        json.append(data)
    
    return json

#calcula a janela de tempo que os dados n foram enviados ao banco
def window_missing(env_data,time):
    json = []
    for data in env_data:
        len_data = len(data['values'])    
        for value in data['values']:
            value[1] = 1
        previous = datetime.strptime(data['values'][0][0],'%Y-%m-%dT%H:%M:%S.%fZ') 
        index = 0
        while(len_data < time):   
            if(index < len_data):  
                current = datetime.strptime(data['values'][index][0],'%Y-%m-%dT%H:%M:%S.%fZ')
                aux = current - previous
                aux = int(aux.total_seconds()/65)
                for i in range(aux):
                    strtime= current+timedelta(minutes=i)
                    data['values'].insert(index+i,[strtime.strftime('%Y-%m-%dT%H:%M:%S.%fZ'),0])
                previous = current
                index = index + int(aux/65) + 1
                len_data = len(data['values'])
            else:
                now = datetime.now()
                last = datetime.strptime(data['values'][len(data['values'])-1][0],'%Y-%m-%dT%H:%M:%S.%fZ')
                aux = now - last
                if(aux.total_seconds() > 65):
                    current = current+timedelta(minutes=1)
                    data['values'].insert(index,[current.strftime('%Y-%m-%dT%H:%M:%S.%fZ'),0])
                    index = index+1
                    len_data = len(data['values'])
                else:
                    current = datetime.strptime(data['values'][0][0],'%Y-%m-%dT%H:%M:%S.%fZ') - timedelta(minutes=1)
                    data['values'].insert(0,[current.strftime('%Y-%m-%dT%H:%M:%S.%fZ'),0])
                    index = index+1
                    len_data = len(data['values'])

        values = data.pop('values')
        data.pop('columns')

        #formatando os dados para enviar ao banco
        for value in values: 
            aux = data.copy()
            aux['fields'] = {'missing_value':value[1]}  
            aux['time'] = value[0]
            json.append(aux) 

    client = InfluxDBClient(host='192.168.12.103', port=8086, username='admin', password='pandasht') 
    client.switch_database('high_density_plots') 
    print(client.write_points(json))
        

    