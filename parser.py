from influxdb import InfluxDBClient
from pprint import pprint


#alguns dashboards do grafana utilizam uma estrutura diferente da padrao env:app:node:io, acabam utilizando apenas node:channel, esse parser recebe o padrao 
#env:app:node:io e retorna o canal daquele node para redirecionarmos para pagina correta.


def get_channel(env,app,node):
    client = InfluxDBClient(host='192.168.12.103', port=8086, username='admin', password='pandasht')
    client.switch_database('ifConfigs')
    result = client.query(f"SELECT ch,measure FROM {node} WHERE app_env='{env}' AND application='{app}'")
    channel = result.raw['series'][0]['values'][0][1]

    return channel
