# Visão geral de funcionamento da aplicação
Cada um dos arquivos tem o nome do seu respectivo dashboard no Grafana (menos os arquivos main_flask e parser, que servem apenas para o funcionamento geral da aplicação), o arquivo main_flask controla toda a aplicacao e recebe as chamadas de funções do grafana, então realiza os cálculos e redireciona o usuário de volta ao seu dashboard com os valores ja calculados e prontos para serem lidos do banco de dados.

Todas essas funções são  calculadas quando o usuário clicar no botão "Calcular" ou "Calcular todos indicadores" no canto superior direito de cada dashboard no Grafana.

Para executar a aplicação basta deixar o script main_flask.py rodando na VM do Grafana, caso seja necessario alterar os links basta mudar o redirect, abaixo irei descrever o funcionamento de cada script individualmente, todas funções de cada script ja estão documentadas no próprio arquivo.

## Data quality analysis
Esse script representa o dashboard de Qualidade dos sinais, existem diversas funções (Fator de compressao, fator de quantizacao, outliers, dados faltantes, intervalo de tempo entre envios e janela de envios), todas são executadas a partir da função write_data(), todos os dados são acumulados em um unico JSON que entao é enviado para o banco, por conta disso a última função executada descarta os dados inuteis do JSON, se for necessário adicionar mais funções a ultima função precisa remover as chaves "name","value" e "columns.

## High density plots
Nos gráficos de alta densidade mostramos o funcionamento de um grupo de nodes, para isso fazemos a normalizacao dos dados utilizando um z-score e os empilhamos, a função forecast() faz a mesma coisa porém para os dados da predição de ponto de orvalho.

## Data quality analysis table
Esse script apenas executa o script "data_quality_analysis" para todos environments e aplicações existentes no banco.

## Parser
Como alguns dashboards do grafana utilizam uma estrutura diferente da padrao env:app:node:io (usam apenas node:channel), esse parser faz a conversão e redireciona o usuário para a estrutura correspondente.
